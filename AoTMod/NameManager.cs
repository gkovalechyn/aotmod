using UnityEngine;
using System.Collections;
using System.IO;

public class NameManager{
	private string playerName;
	private string playerGuild;
	private string displayName;

	public NameManager(){
		this.loadData();
	}

	public string getPlayerName(){
		return this.playerName;
	}

	public string getPlayerGuild(){
		return this.playerGuild;
	}

	public string getPlayerDisplayName(){
		return this.displayName;
	}

	public void setDisplayName(string ndn){
		this.displayName = ndn;
	}

	public void setName(string nn){
		this.playerName = nn;
	}

	public void setGuild(string guild){
		this.playerGuild = guild;
	}

	private void loadData(){
		ConfigManager cm = new ConfigManager("./config.cfg");
		this.displayName = cm.get("displayName").Replace("\\n", "\n");
		this.playerName = cm.get("name").Replace("\\n", "\n");
		this.playerGuild = cm.get ("guild").Replace("\\n", "\n");
	}
}

