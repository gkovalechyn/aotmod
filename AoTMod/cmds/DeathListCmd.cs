using UnityEngine;
using System.Collections;

public class DeathListCmd : ICommand{
	//dl <ID>
	//dl -r <index>
	//dl
	public bool cmd(string[] args, FengGameManagerMKII gm){
		if (args.Length == 0){
			int i = 0;

			ModMain.instance.sendToPlayer("Is death list enabled: " + ModMain.instance.getModMainThread().isDeathListEnabled());
			foreach(string s in ModMain.instance.getModMainThread().getDeathList()){
				ModMain.instance.sendToPlayer("" + (i++) + "- " + s);
			}

			return true;
		}

		if (args[0].Equals("on", System.StringComparison.OrdinalIgnoreCase)){
			ModMain.instance.getModMainThread().setDeathListEnabled(true);
			return true;
		}

		if (args[0].Equals("off", System.StringComparison.OrdinalIgnoreCase)){
			ModMain.instance.getModMainThread().setDeathListEnabled(false);
			return true;
		}

		if (args[0].Equals("-r")){
			ModMain.instance.getModMainThread().removeDeathList(int.Parse(args[1]));
			return true;
		}

		foreach(PhotonPlayer p in PhotonNetwork.otherPlayers){
			if ("" + p.ID == args[0]){
				ModMain.instance.getModMainThread().addToDeathList((string) p.customProperties[PhotonPlayerProperty.name]);
				return true;
			}
		}

		return false;
	}
}

