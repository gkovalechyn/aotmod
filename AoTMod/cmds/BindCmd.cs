using UnityEngine;
using System;
using System.Collections;

//bind <key> <cmd>
//bind k /kill *
public class BindCmd : ICommand{
	private static string[] help = new string[]{
		"/bind - Show help and keybinds.",
		"/bind <key> <command> - binds that key to that command.",
		"/bind -r <key> - Removes that keybind."
	};

	public bool cmd(string[] args, FengGameManagerMKII gm){
		if (args.Length == 0){
			ModMain.instance.sendToPlayer(help);
			Hashtable bound = ModMain.instance.getBoundCommands();
			string[] messages = new string[bound.Count];
			int i  = 0;

			foreach(DictionaryEntry entry in bound){
				messages[i++] = "[" + entry.Key + "] " + entry.Value;
			}

			ModMain.instance.sendToPlayer(messages);
			return true;
		}

		if (args[0].Equals("-r")){
			ModMain.instance.unbind(args[1][0]);
			ModMain.instance.sendToPlayer("Keybind cleared.");
			return true;
		}else{
			string[] cmd = new string[args.Length - 1];
			Array.Copy(args, 1, cmd, 0, cmd.Length);

			//ModMain.instance.bind(args[1][0], string.Join(cmd, ' '));
			ModMain.instance.sendToPlayer("Command bound.");
			return true;
		}
	}
}

