using UnityEngine;
using System.Collections;

public class BlockSpawnCmd : ICommand{

	//bs <.size> <type> -- add to blocked list
	//bs -r <.size> <type> -- remove from blocked list
	//bs -- show blocked spawns
	private string[] help = {
		"/bs <.size> <type> -- add to blocked list",
		"/bs -r <.size> <type> -- remove from blocked list",
		"/bs -- show blocked spawns"
	};
	public bool cmd(string[] args, FengGameManagerMKII gm){
		if (args.Length == 0){
			ModMain.instance.sendToPlayer(help);
			ModMain.instance.sendToPlayer("Blocked spawns:");
			ModMain.instance.sendToPlayer(ModMain.instance.getDisallowedTitans().ToArray());
			return true;
		}
		
		string sc = "";

		if (args[0].Equals("-r")){
			sc = args[1] + args[2];
			ModMain.instance.getDisallowedTitans().Remove(sc);
			ModMain.instance.sendToPlayer("Titan spawning allowed.");
		}else{
			sc = args[0] + args[1];
			ModMain.instance.getDisallowedTitans().Add(sc);
			ModMain.instance.sendToPlayer("Titan spawning disallowed.");
		}

		return true;
	}
}

