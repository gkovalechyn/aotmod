using UnityEngine;
using System.Collections;

public class DestroyCmd : ICommand{
	public bool cmd(string[] args, FengGameManagerMKII gm){
		PhotonPlayer player = null;

		foreach(PhotonPlayer p in PhotonNetwork.otherPlayers){
			if ("" + p.ID == args[0]){
				player = p;
				break;
			}
		}
		if (player != null){
			PhotonNetwork.DestroyPlayerObjects(player);
			ModMain.instance.sendToPlayer("Destroyed objects.");
		}else{
			ModMain.instance.sendToPlayer("Player with ID " + args[0] + " not found");
		}
		return true;
	}
}

