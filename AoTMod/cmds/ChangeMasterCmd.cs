using UnityEngine;
using System.Collections;
using System.Reflection;

public class ChangeMasterCmd : ICommand{
	public bool cmd(string[] args, FengGameManagerMKII gm){
		ModMain.instance.log("Changing master client to player with id " + args[0]);

		foreach(PhotonPlayer player in PhotonNetwork.playerList){

			if (("" + player.ID) == args[0]){
				PhotonNetwork.SetMasterClient(player);
				return true;
			}
		}
		
		return true;
	}
}

