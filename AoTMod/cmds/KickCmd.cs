using UnityEngine;
using System;
using System.Collections;
using System.Reflection;

public class KickCmd : ICommand{
	public bool cmd(string[] args, FengGameManagerMKII gm){
		ModMain.instance.log("kicking player with id " + args[0]);

		if (args[0].Equals("-o")){//o = original
			ModMain.instance.sendToAll("/kick #" + args[1]);
			return true;
		}

		else if (args[0].Equals("-f")){
			for (int i =0;	i < Math.Ceiling((float)PhotonNetwork.room.playerCount / 2); i++){
				gm.photonView.RPC("Chat", PhotonTargets.All, new object[]{"/kick #" + args[1], "GUEST" + UnityEngine.Random.Range(0, 10000)});
			}

			return true;
		}

		foreach(PhotonPlayer player in PhotonNetwork.playerList){
			ModMain.instance.log("Checking " + player.customProperties[PhotonPlayerProperty.name] + " with ID: " + player.ID);
			if (("" + player.ID) == args[0]){
				string playerName = (string) player.customProperties[PhotonPlayerProperty.name];

				if (ModMain.instance.getConfig().get("showMessagesToPlayers").Equals("True", StringComparison.OrdinalIgnoreCase) && PhotonNetwork.player.isMasterClient){
					ModMain.instance.sendToAll(Colorizer.colorize("Kicking player " + playerName, Colorizer.Color.YELLOW, true));
				}
				try{
					PhotonNetwork.CloseConnection(player);
				}catch (System.Exception e){
					ModMain.instance.log(e);
				}
				//kickPlayer((string) player.customProperties[PhotonPlayerProperty.name], gm);
				//kickPlayer2(player.ID);
				//ModMain.instance.log("Kick with master client change");
				
				return true;
			}
		}
		
		return true;
	}

	private void kickPlayer(string name,  FengGameManagerMKII gm){
		MethodInfo mInfo = gm.GetType().GetMethod("kickPhotonPlayer", BindingFlags.Instance | BindingFlags.NonPublic);
		mInfo.Invoke(gm, new object[]{name});
	}

	private void kickPlayer2(int id){
		Type type = typeof(PhotonNetwork);
		ModMain.instance.log("Got the PhotonNetwork type");
		FieldInfo networkingPeerField = type.GetField("networkingPeer", BindingFlags.Static | BindingFlags.NonPublic);
		object networkingPeer = networkingPeerField.GetValue(null);

		ModMain.instance.log("Networking peer: " + networkingPeer);
		ModMain.instance.log("got the networkPeer value");
		/*
		MethodInfo raiseOpMethod = networkingPeer.GetType().GetMethod("OpRaiseEvent", 
		                                                              BindingFlags.Instance | BindingFlags.Public,
		                                                              null,
		                                                              new Type[]{typeof(Byte), typeof(Hashtable), typeof(Boolean), typeof(Byte), typeof(Int32[])},
																	  null);
		*/
		MethodInfo raiseOpMethod = getRaiseEventMethod(networkingPeer);
		ModMain.instance.log("got the RaiseOp method");
		ModMain.instance.log("Method: " + raiseOpMethod);
		try{
			raiseOpMethod.Invoke(networkingPeer, new object[]{(byte)0xCB, null, true, (byte)0, new int[]{id}});
		}catch(Exception e){
			ModMain.instance.log(e);
		}
		ModMain.instance.log("After invoke of the RaiseOP method");
	}

	private  MethodInfo getRaiseEventMethod(object nwp){
		MethodInfo[] methods = nwp.GetType().GetMethods();

		foreach (MethodInfo method in methods){
			if (method.Name == "OpRaiseEvent" && method.GetParameters().Length == 5){
				ParameterInfo[] infos = method.GetParameters();
				ModMain.instance.log("ParameterInfo[4]" + infos[4]);
				if (infos[4].ParameterType == typeof(Int32[])){
					return method;
				}
			}
		}

		return null;
	}
}

