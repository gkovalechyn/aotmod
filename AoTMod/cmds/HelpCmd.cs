using UnityEngine;
using System.Collections;

public class HelpCmd : ICommand{
	private string[] help1 = {
		"/kick [-o] <playerId> - kicks the player.",
		"/respawn <playerId|*> - Respawns that player or all players.",
		"/cMaster <playerId> - Changes the master client.",
		"/restart - Restarts the game.",
		"/info - Mod information.",
		"/greeter - Shows the greeter information",
		"/set <gas|blades> <on|off> - enables/disables usage",
		"/set damage <damage> - sets the target average damage",
		"/msg <id> <message> - Messages a player",
		"/kill <id|*> - Kills the player",
		"/spawn <amount> <id> - Spawns <amount> titans near the player <id>"};

	private string[] help2 = {
		"/th <set|modifier> <args> - Titan health mod commands.",
		"/reset - Reset stats.(!!SEE!! /reset help)",
		"/bs [-r] <.size> <type> - Blocks titan spawning",
		"/endGame <id|*> - Ends the game for that player or all players",
		"/gravity <on|off> - Enables/Disables gravity",
		"/cl <on|off> - Enables/Disables the chat log",
		"/me <message>",
		"/ban <ID> - Bans a player with that ID.",
		"/ban -r <index> - Unban player at index(see list)",
		"/ban - List banned players",
		"/ban [-s] <name> - Bans that name part."
	};

	private string[] help3 = {
		"/destroy <id> - Destroy all the prefabs of that player.",
		"/eren [id] - Transforms a player into titan eren.",
		"/transform <scale|type> <value> - Transforms all the titans.",
		"/persona <1-3> - Switch between personas and custom characters.",
		"/dl [on|off] - Shows players on the death list.",
		"/dl <id> - Adds id to death list.",
		"/dl -r <index> - Removes from death list at that index.",
		"/whois <id> - Shows information about that player.",
		"/bind - Binds a command to a key.",
		"/tc - Titan control commands.",
		"/ch - Championship commands."

	};

	private string[] help4 = {
		"/sc - Spawn control command.",
		"/room - Change room settings",
		"/destroyList - Keep destroying players prefabs.",
		"/crash <id> - Crash a player's game. (Maybe works?)",
		"/autoRespawn - Toggles the auto respawn of players",
		"/nameChanger - Controls the Name Changer",
		"/raw <message> - Sends a raw message to everyone.",
		"/addForce - Adds a force to a player",
		"/lag - Lag other players",
		"/horse - Spawns a horse.",
		"/godMode <on|off> - Toggle god mode."
	};

	public HelpCmd(){
		Colorizer.Color[] colors = new Colorizer.Color[]{
			Colorizer.Color.GREEN,
			Colorizer.Color.YELLOW
		};

		Colorizer.colorize(help1, colors, true);
		Colorizer.colorize(help2, colors, true);
		Colorizer.colorize(help3, colors, true);
		Colorizer.colorize(help4, colors, true);
	}

	public bool cmd(string[] args, FengGameManagerMKII gm){
		if (args.Length < 1){
			ModMain.instance.sendToPlayer("/help <1-4>");
			return true;
		}

		if (args[0] == "1"){
			ModMain.instance.sendToPlayer(help1);
		}else if (args[0] == "2"){
			ModMain.instance.sendToPlayer(help2);
		}else if (args[0] == "3"){
			ModMain.instance.sendToPlayer(help3);
		}else{
			ModMain.instance.sendToPlayer(help4);
		}

		return  true;
	}
}

