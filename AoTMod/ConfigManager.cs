using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using System;
using System.IO;

public class ConfigManager{
	private Dictionary<string, object> configValues = new Dictionary<string, object>();
	private string filePath;

	public ConfigManager(string file){
		this.filePath = file;
		this.load();
	}

	public string get(string path){
		object res = null;
		configValues.TryGetValue(path, out res);
		return (string) res;
	}

	public bool getBool(string path){
		object res = null;
		configValues.TryGetValue(path, out res);

		if (res != null && !string.IsNullOrEmpty((string) res)){
			return ((string)res).Equals("true", StringComparison.OrdinalIgnoreCase);
		}else{
			return false;
		}
	}

	public void set(string path, string value){
		try{
			this.configValues.Add(path, value);
		}catch(ArgumentException){
			this.configValues.Remove(path);
			this.configValues.Add(path, value);
		}
	}

	public void set(string path, List<object> list){
		try{
			this.configValues.Add(path, list);
		}catch(ArgumentException){
			this.configValues.Remove(path);
			this.configValues.Add(path, list);
		}
	}

	private void load(){
		StreamReader fileIn;
		int line = 1;

		if (!File.Exists(this.filePath)){
			StreamWriter fileOut = File.CreateText("./config.cfg");
			
			fileOut.WriteLine("name=<color=#000000>Test</color>");
			fileOut.WriteLine("displayName=[000000]Test");
			fileOut.WriteLine("guild=[000000]Test guild");
			fileOut.WriteLine("averageDamage=800");
			fileOut.WriteLine("showMessagesToPlayers=true");
			fileOut.WriteLine("blockedSpawns=[Crawler]");
			fileOut.WriteLine("enableModMessage=False");
			fileOut.WriteLine("modMessage=Hello, this is a mod test server :3");
			fileOut.WriteLine("killTitanName=Titan");
			fileOut.WriteLine("chatLogEnabled=true");
			fileOut.WriteLine("persona1=Persona1");
			fileOut.WriteLine("persona1DN=Persona1");
			fileOut.WriteLine("persona1Guild=Guild1");
			fileOut.WriteLine("persona2=Persona2");
			fileOut.WriteLine("persona2DN=Persona2");
			fileOut.WriteLine("persona2Guild=Guild2");
			fileOut.WriteLine("persona3=Persona3");
			fileOut.WriteLine("persona3DN=Persona3");
			fileOut.WriteLine("persona3Guild=Guild3");
			fileOut.Flush();
			fileOut.Close();
		}

		fileIn = File.OpenText(this.filePath);

		while(fileIn.Peek() >= 0){
			string fullLine = fileIn.ReadLine().Trim();
			string[] parts;

			if (string.IsNullOrEmpty(fullLine) || fullLine.StartsWith("/")){
				line++;
				continue;
			}

			parts = fullLine.Split('=');

			if (parts.Length == 1){
				ModMain.instance.log("Malformed configuration value at line: " + line + '(' + fullLine + ')');
			}else{
				for(int i = 2; i < parts.Length; i++){
					parts[1] += "=" + parts[i];
				}
			}

			if (parts[1].Equals("[")){
				List<string> list = new List<string>();
				string readLine;

				while(!(readLine = fileIn.ReadLine()).Equals("]")){
					list.Add(readLine.Replace("\\r\\n", "\r\n").Replace("\\n", "\n").Replace("\\t", "\t").Replace("\\\\", "\\"));
					line++;
				}
				//Increase the line because of the ]
				line++;
				this.configValues[parts[0]] = list;
			}else if (parts[1].Equals("\\[")){
				this.configValues[parts[0]] =  "[";
			}else{
				this.configValues[parts[0]] = parts[1];
			}
		}
	}

	public void save(){
		try{
			StreamWriter fileOut = new StreamWriter(this.filePath);

			foreach(KeyValuePair<string, object> entry in configValues){
				if (entry.Value is List<string>){
					List<string> list = (List<string>) entry.Value;

					fileOut.WriteLine(entry.Key + "=[");
					foreach(object o in list){
						fileOut.WriteLine(o);
					}

					fileOut.WriteLine(']');
				}else{
					if (entry.Value.Equals("[")){
						fileOut.WriteLine(entry.Key + "=\\[");
					}else{
						fileOut.WriteLine(entry.Key + "=" + entry.Value);
					}
				}

			}

			fileOut.Flush();
			fileOut.Close();
		}catch(IOException){

		}
	}

	public List<string> getStringList(string path){
		object result;
		this.configValues.TryGetValue(path, out result);

		return result != null ? (List<string>) result : null;
	}

	public Dictionary<string, object>.KeyCollection getKeyList(){
		return this.configValues.Keys;
	}
}

