using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Threading;

public class MainModThread{
	private Hashtable bannedNames = new Hashtable();
	private List<string> deathList = new List<string>();
	private List<int> destroyList  = new List<int>();

	private readonly object bannedLock = new object();
	private readonly object deathLock = new object();
	private readonly object destroyLock = new object();

	private volatile bool deathListEnabled = false;
	private volatile bool destroyListEnabled = false;

	private Hashtable nullNamePasses = new Hashtable();
	private int allowedPasses = 2;

	private bool toStop = false;
	private bool running = false;

	public volatile bool autoRespawnEnabled = false;

	private FengGameManagerMKII gm;
	private RespawnTask task;
	private ModMain mod;

	public MainModThread (ModMain mod) {
		this.mod = mod;
	}
	
	public void banName(string name, BanType type){
		lock(bannedLock){
			bannedNames[ModMain.stripColorCodes(name)] = type;
		}
	}

	public void unban(int index){
		object[] keys = new object[bannedNames.Count];
		bannedNames.Keys.CopyTo(keys, 0);

		lock(bannedLock){
			bannedNames.Remove(keys[index]);
		}
	}

	public Hashtable getBannedNames(){
		return this.bannedNames;
	}

	public void start(){
		if (!this.running){
			new Thread(this.run).Start();
			this.gm = this.mod.getGameManager();
			this.task = new RespawnTask(this.gm);
		}
	}
	private void run(){
		this.running = true;
		bool dead = false;
		try{
			while(!this.toStop){
				if (PhotonNetwork.connected){
					bool hasDeadPlayers = false;

					if (PhotonNetwork.isMasterClient){
						lock(bannedLock){
							doBanAndDeathChecks();
						}
						/*
						if (this.isDeathListEnabled()){
							this.doDeathCheckOnly();
						}
						*/
					}else if (this.isDeathListEnabled()){
						doDeathCheckOnly();
					}

					this.doDestroyCheck();

					foreach(PhotonPlayer player in PhotonNetwork.playerList){
						object playerIsDead = player.customProperties[PhotonPlayerProperty.dead];

						if (playerIsDead != null && ((bool) playerIsDead)){
							hasDeadPlayers = true;
							break;
						}
					}

					if (this.autoRespawnEnabled && hasDeadPlayers){
						ModMain.instance.getTaskManager().addLateUpdateTask(this.task);
					}
				}

				Thread.Sleep(1000);
			}
		}catch(System.Exception e){
			dead = true;
			ModMain.instance.log("Main mod thread died. Exception: ");
			ModMain.instance.log(e);
			ModMain.instance.log("Restarting Thread.");
		}
		this.running = false;

		if (dead){
			this.start();
		}
	}

	private void doDeathCheckOnly(){
		foreach (PhotonPlayer p in PhotonNetwork.otherPlayers){
			string name = (string) p.customProperties[PhotonPlayerProperty.name];
			string stripped;

			if ((name == null  || name == string.Empty) && canPass(p.ID, name)){
				continue;
			}

			stripped = ModMain.stripColorCodes(name);
			doDeathCheck(stripped, p);
		}
	}

	private void doBanAndDeathChecks(){
		foreach (PhotonPlayer p in PhotonNetwork.otherPlayers){
			try{
				string name = (string) p.customProperties[PhotonPlayerProperty.name];
				string stripped;
				
				if ((name == null  || name == string.Empty) && canPass(p.ID, name)){
					continue;
				}
				//if the name is still blank which means the player has a blank name
				//also, this will only be true if the player has been for more than 5
				//seconds on the server and has a blank name
				if (name == null || name == string.Empty){
					ModMain.instance.sendToPlayer(p, "Sorry, blank names not allowed!");
					PhotonNetwork.CloseConnection(p);
					continue;
				}

				stripped = ModMain.stripColorCodes(name).Trim();

				if (stripped == null || stripped == string.Empty){
					ModMain.instance.sendToPlayer(p, "Sorry, blank names not allowed!");
					PhotonNetwork.CloseConnection(p);
				}

				if (this.deathListEnabled){
					doDeathCheck(stripped, p);
				}
				
				if (isBanned(stripped)){
					PhotonNetwork.CloseConnection(p);
				}
				
			}catch(System.Exception e){
				ModMain.instance.log(e);
			}
		}
	}

	public bool isBanned(string stripped){
		foreach(string bannedName in bannedNames.Keys){
			switch((BanType)bannedNames[bannedName]){
				case BanType.CONTAINS_PART:
					if(stripped.Contains(bannedName)){
						return true;
					}
					break;
				case BanType.FULL_NAME:
					if (stripped.Equals(bannedName)){
						return true;
					}
					break;
				case BanType.GUILD_FULL:
					return false;
					//@TODO
					//banned = stripped.Equals(bannedName);
				case BanType.GUILD_PART:
					return false;
					//@TODO
					//banned = stripped.Equals(bannedName);
			}
		}

		return false;
	}

	public bool isDeathListEnabled(){
		return this.deathListEnabled;
	}

	public void setDeathListEnabled(bool val){
		this.deathListEnabled = val;
	}

	public List<string> getDeathList(){
		return this.deathList;
	}

	public void removeDeathList(int index){
		lock(this.deathLock){
			this.deathList.RemoveAt(index);
		}
	}

	public void addToDeathList(string name){
		string stripped = ModMain.stripColorCodes(name);

		lock(this.deathLock){
			this.deathList.Add(stripped);
		}
	}
	private bool canPass(int id, string name){
		if (name == null || name == string.Empty){
			if (this.nullNamePasses.ContainsKey(id)){
				this.nullNamePasses[id] = (int)this.nullNamePasses[id] + 1;
				
				if ((int)this.nullNamePasses[id] > this.allowedPasses){
					this.nullNamePasses.Remove(id);
					return false;
				}else{
					return true;
				}
			}else{
				this.nullNamePasses[id] = 0;
				return true;
			}

		}else if (this.nullNamePasses.ContainsKey(id)){
			this.nullNamePasses.Remove(id);
			return true;
		}else{
			return true;
		}
	}

	private void doDeathCheck(string player, PhotonPlayer pp){
		lock(deathLock){
			foreach(string s in deathList){
				if (s.Equals(player)){
					foreach(HERO h in GameObject.FindObjectsOfType<HERO>()){
						if (h.photonView.ownerId == pp.ID){
							h.photonView.RPC("netDie2", PhotonTargets.All ,new object[]{-1, ""});
						}
					}
				}
			}
		}
	}

	private void doDestroyCheck(){
		lock(this.destroyLock){
			foreach(int id in this.destroyList){
				if (PhotonPlayer.Find(id) == null){
					this.destroyList.Remove(id);
					continue;
				}

				PhotonNetwork.DestroyPlayerObjects(id);
			}
		}
	}


	public bool isToStop(){
		return this.toStop;
	}

	public void stop(){
		this.toStop = true;
	}

	public bool isRunning(){
		return this.running;
	}

	public void addToDestroyList(int id){
		lock(this.destroyLock){
			this.destroyList.Add(id);
		}
	}

	public void removeFromDestroyList(int id){
		lock(this.destroyLock){
			this.destroyList.Remove(id);
		}
	}

	public bool DestroyListEnabled{
		get{
			return this.destroyListEnabled;
		}

		set {
			this.destroyListEnabled = value;
		}
	}

	public List<int> getDestroyList(){
		return this.destroyList;
	}

	public enum BanType{
		FULL_NAME = 0,
		CONTAINS_PART = 1,
		GUILD_FULL = 2,
		GUILD_PART = 3
	}

	private class RespawnTask : Task{
		private readonly FengGameManagerMKII gm;

		public RespawnTask (FengGameManagerMKII gm){
			this.gm = gm;
		}
		
		public bool execute (){
			gm.photonView.RPC("respawnHeroInNewRound", PhotonTargets.All, new object[0]);
			return true;
		}
	}
}

